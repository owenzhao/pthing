package com.parussoft.things;

/**
 * Created by 肇鑫 (Owen Zhao) on 14-1-15.
 */
public class BundleConst {

  private BundleConst() {}

  public static final String END_TIME = "end_time";
  public static final String TIME_LENGTH ="time_length";
}
