package com.parussoft.things;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ListFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.parussoft.things.core.DBHelper;
import com.parussoft.things.core.TimeUpReceiver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.parussoft.things.core.DBConst.ID;
import static com.parussoft.things.core.DBConst.NAME;
import static com.parussoft.things.core.DBConst.START_TIME;
import static com.parussoft.things.core.DBConst.TABLE_NAME;
import static com.parussoft.things.core.DBConst.TIME_LENGTH;

public class MainActivity extends Activity {
  public final static boolean DEBUG = false;

  private static final String ABOUT_DIALOG_IS_OPEN = "about_dialog_is_open";

  private AlertDialog aboutDialog = null;
  private boolean aboutDialogOpenLock = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (savedInstanceState == null) {
      //fragment = new MainFragment();
      getFragmentManager().beginTransaction()
          .add(R.id.container, new MainFragment())
          .commit();
    }
    else if (savedInstanceState.containsKey(ABOUT_DIALOG_IS_OPEN)) {
      aboutDialogOpenLock = savedInstanceState.getBoolean(ABOUT_DIALOG_IS_OPEN);
    }

    if (aboutDialogOpenLock) showAboutDialog();
  }

  @Override
  public boolean onNavigateUp() {
    getFragmentManager().popBackStack();
    return true;
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putBoolean(ABOUT_DIALOG_IS_OPEN, aboutDialogOpenLock);

    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    if (aboutDialog != null && aboutDialog.isShowing()) {
      aboutDialog.dismiss();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up startButton, so long
    // as you specify a parent activity in AndroidManifest.xml.
    switch (item.getItemId()) {
      case R.id.action_about:
        showAboutDialog();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void showAboutDialog() {
    DialogInterface.OnClickListener onClickListener =
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            aboutDialogOpenLock = false;
          }
        };
    DialogInterface.OnCancelListener onCancelListener =
        new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
            aboutDialogOpenLock = false;
          }
        };

    aboutDialog = new AlertDialog.Builder(this)
        .setCancelable(true)
        .setNeutralButton(android.R.string.ok, onClickListener)
        .setOnCancelListener(onCancelListener)
        .setTitle(R.string.action_about)
        .setMessage(R.string.about_message)
        .create();
    aboutDialog.show();
    aboutDialogOpenLock = true;
  }

  /**
   * A placeholder fragment containing a simple view.
   */
  public static class MainFragment extends Fragment
      implements ParusThing.Status {
    private static final String TIME_LENGTH_SETTING = "time_length_setting";
    private static final String COUNTDOWN_CLOCK_LOCK = "countdown_clock_lock";
    private static final String FINISH_TIME = "finish_time";
    private static final String END_TIME = "end_time";

    private Activity activity;
    private SharedPreferences settings = null;
    private ParusThing parusThing = null;

    private TextView endTimeTextView;
    private TextView clockTextView;
    private TextView timeLengthTextView;
    private Button startButton;
    private Button cancelButton;
    private CountDownTimer timer;

    private boolean countdownClockLock = false; //locked when a job is running.

    public MainFragment() {}

    @Override
    public void onAttach(Activity activity) {
      super.onAttach(activity);

      this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//debug
      if (DEBUG) {
        Log.d("MainFragment is ", "onCreateView().");
        Log.d("getView() == null is", String.valueOf(getView() == null));
      }
//get a ParusThing instance
      if (parusThing == null) {
        parusThing = new ParusThing(this, activity);
      }
//set default settings
      if (settings == null) {
        settings = PreferenceManager.getDefaultSharedPreferences(activity);
        if (!settings.contains(TIME_LENGTH_SETTING)) {
          settings.edit().putInt(TIME_LENGTH_SETTING, 30).commit();
        }
      }
//restore state if needed
      if (savedInstanceState != null &&
          (countdownClockLock =
              savedInstanceState.containsKey(COUNTDOWN_CLOCK_LOCK))) {
        parusThing.finishTime = savedInstanceState.getLong(FINISH_TIME);
        parusThing.endTime = savedInstanceState.getLong(END_TIME);
      }
//inflate fragment view
      View mainView = inflater.inflate(R.layout.fragment_main, container, false);
//get views in xml
      clockTextView = (TextView) mainView.findViewById(R.id.clock_textView);
      timeLengthTextView = (TextView) mainView.findViewById(
          R.id.time_length_textView);
      endTimeTextView = (TextView) mainView.findViewById(
          R.id.end_time_textView);
      startButton = (Button) mainView.findViewById(R.id.startButton);
      cancelButton = (Button) mainView.findViewById(R.id.cancelButton);
//format views
      Typeface typeface = Typeface.createFromAsset(activity.getAssets(),
          "AndroidClock_Highlight.ttf");
      clockTextView.setTypeface(typeface);
      timeLengthTextView.setTypeface(typeface);
      endTimeTextView.setTypeface(typeface);
      endTimeTextView.setVisibility(
          countdownClockLock ? View.VISIBLE : View.INVISIBLE
      );
      startButton.setVisibility(
          countdownClockLock ? View.INVISIBLE : View.VISIBLE
      );
      cancelButton.setVisibility(
          countdownClockLock ? View.VISIBLE : View.INVISIBLE
      );
      startButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          v.setVisibility(View.INVISIBLE);
          cancelButton.setVisibility(View.VISIBLE);
          parusThing.start();
        }
      });
      cancelButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          v.setVisibility(View.INVISIBLE);
          startButton.setVisibility(View.VISIBLE);
          parusThing.cancel();
        }
      });
//set menu
      setHasOptionsMenu(true);

      return mainView;
    }

    @Override
    public void onResume() {
      super.onResume();
//debug
      if (DEBUG) {
        Log.d("MainFragment is ", "onResume()");
        Log.d("finishTime at onResume() is ", String.valueOf(parusThing.finishTime));
      }
//restore start startButton when time is up.
      if (countdownClockLock) {
        parusThing.start();
      }
//timer update the ui 60 times per second
      timer = new CountDownTimer(24 * 60 * 60 * 1000, 1000 / 60) {//24 hours
        Calendar calendar = Calendar.getInstance();
        DateFormat wallClockDateFormat =
            new SimpleDateFormat("HH:mm:ss", Locale.US);
        int light = Color.rgb(0x66, 0x66, 0x66);
        int dark = Color.rgb(0x99, 0x33, 0xcc);
        long leftTime = 0;

        @Override
        public void onTick(long millisUntilFinished) {
          leftTime = parusThing.finishTime - SystemClock.elapsedRealtime();
          //change endTimeTextView's textColor when running
          if (countdownClockLock && leftTime >= 0) {
            calendar.setTimeInMillis(leftTime);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            int seconds = calendar.get(Calendar.SECOND);
            endTimeTextView.setTextColor((seconds % 2 == 1) ? light : dark);
          }
          else {//show desktop clock
            calendar.setTimeInMillis(System.currentTimeMillis());
          }

          clockTextView.setText(wallClockDateFormat.format(calendar.getTime()));
        }

        @Override
        public void onFinish() {
          //do nothing
        }
      }.start();
    }

    @Override
    public void onPause() {
      super.onPause();
//debug
      if (DEBUG) {
        Log.d("MainFragment is ", "onPause()");
        Log.d("finishTime at onPause is ", String.valueOf(parusThing.finishTime));
      }
//stop ui updating
      timer.cancel();
      timer = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
      if (countdownClockLock) {
        outState.putBoolean(COUNTDOWN_CLOCK_LOCK, true);
        outState.putLong(FINISH_TIME, parusThing.finishTime);
        outState.putLong(END_TIME, parusThing.endTime);
      }

      super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
      super.onDestroyView();
//debug
      if (DEBUG) {
        Log.d("MainFragment is", "onDestroyView()");
      }
    }

    @Override
    public void onDestroy() {
      super.onDestroy();
//debug
      if (DEBUG) {
        Log.d("MainFragment is ", "onDestroy().");
      }
//release alarm
      if (countdownClockLock) {
        parusThing.cancelAlarm();
      }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
      menu.setGroupEnabled(R.id.group_time_length, !countdownClockLock);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
      inflater.inflate(R.menu.menu, menu);
      MenuItem menuItem = null;
      switch (parusThing.timeLength) {
        case 25:
          menuItem = menu.findItem(R.id.menu_25_minutes);
          break;
        case 30:
          menuItem = menu.findItem(R.id.menu_30_minutes);
          break;
        case 40:
          menuItem = menu.findItem(R.id.menu_40_minutes);
          break;
        case 45:
          menuItem = menu.findItem(R.id.menu_45_minutes);
          break;
        case 50:
          menuItem = menu.findItem(R.id.menu_50_minutes);
          break;
        default:
      }
      if (menuItem != null) {
        menuItem.setChecked(true);
      }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      item.setEnabled(false); //in case user click it too quickly
      boolean has_dealt = true;
      int select_time_length = 0;

      switch (item.getItemId()) {
        case R.id.menu_25_minutes:
          select_time_length = 25;
          break;
        case R.id.menu_30_minutes:
          select_time_length = 30;
          break;
        case R.id.menu_40_minutes:
          select_time_length = 40;
          break;
        case R.id.menu_45_minutes:
          select_time_length = 45;
          break;
        case R.id.menu_50_minutes:
          select_time_length = 50;
          break;
        case R.id.action_show_records:
          activity.getFragmentManager()
              .beginTransaction()
              .replace(R.id.container, new RecordsFragment())
              .addToBackStack(null)
              .commit();
          break;
        default:
          has_dealt = super.onOptionsItemSelected(item);
      }

      if (select_time_length != 0) {
        item.setChecked(true);
        settings.edit().putInt(TIME_LENGTH_SETTING, select_time_length).commit();
        timeLengthTextView.setText(getTimeLengthStringResource());
        parusThing.timeLength = select_time_length;
      }

      item.setEnabled(true);

      return has_dealt;
    }

    private String getTimeLengthStringResource() {
      return String.format(getString(R.string.minutes), settings.getInt(TIME_LENGTH_SETTING, 30));
    }

    private void showEndTime() {
      Date date = new Date(parusThing.endTime);
      DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
      endTimeTextView.setText(dateFormat.format(date));
      endTimeTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onParusThingStart() {
      countdownClockLock = true;
      showEndTime();
    }

    @Override
    public void onParusThingCancel() {
      endTimeTextView.setVisibility(View.INVISIBLE);
      countdownClockLock = false;
    }
  }

  /*
    ParusThing class is the class where really runs in console.
    This class is the core of this app.
     */
  private static class ParusThing {
    interface Status {
      public void onParusThingStart();

      public void onParusThingCancel();
    }

    private Status status = null;
    private Context context;
    private int timeLength; //should be 25, 30, 40, 45, 50. minutes
    private long finishTime; //hold when job will finish in elapsed time.
    private long endTime; //hold when job will end in wall clock
    private static int times = 0; //hold the pending intent times

    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    public ParusThing(Object object, Context context) {
//get interface
      try {
        this.status = (Status) object;
      } catch (ClassCastException e) {
        Log.e("Error", object.getClass().getName() +
            " must implements Status interface of " +
            this.getClass().getName());
      }
      this.context = context;
//set default values
      this.timeLength = 0;
      this.endTime = 0;
//read settings
      this.timeLength = PreferenceManager.getDefaultSharedPreferences(context)
          .getInt(MainFragment.TIME_LENGTH_SETTING, 30);
    }

    public void start() {
      if (status != null) {
//set finishTime if it is not set yet
        if (finishTime == 0) { //you don't want to recalculate the time
//debug
          if (DEBUG) {
            Log.d("Should not run ", "after onPause()");
          }
          finishTime = SystemClock.elapsedRealtime() + timeLength * 60 * 1000;
          endTime = System.currentTimeMillis() + timeLength * 60 * 1000;
          ++times; //hold the sender of PendingIntent
//time length in testing
          if (DEBUG) {
            finishTime = SystemClock.elapsedRealtime() + 10 * 1000;//for test
            endTime = System.currentTimeMillis() + 10 * 1000;//for test
          }
        }
//set future alarm
        setAlarm();
//call back ui part
        status.onParusThingStart();
      }
    }

    public void cancel() {
      if (status != null) {
//reset finishTime and endTime
        finishTime = 0;
        endTime = 0;
//cancel alarm
        cancelAlarm();
//call back ui part
        status.onParusThingCancel();
      }
    }

    private void setAlarm() {
      alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
      Intent intent = new Intent(context, TimeUpReceiver.class);
      Bundle bundle = new Bundle();
      bundle.putLong(BundleConst.END_TIME, endTime);
      bundle.putInt(BundleConst.TIME_LENGTH, timeLength);
      intent.putExtras(bundle);
      pendingIntent = PendingIntent.getBroadcast(context, times, intent, 0);
      alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, finishTime, pendingIntent);
//debug
      if (DEBUG) {
        Log.d("endTime in bundle is", String.valueOf(endTime));
        Log.d("timeLength in bundle is ", String .valueOf(timeLength));
      }
    }

    private void cancelAlarm() {
      alarmManager.cancel(pendingIntent);
    }
  }

  public static class RecordsFragment extends ListFragment {
    private Activity activity;

    @Override
    public void onAttach(Activity activity) {
      super.onAttach(activity);
      this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_show_records, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);

      if (activity.getActionBar() != null) {
        activity.getActionBar().setDisplayHomeAsUpEnabled(true);
      }
    }

    @Override
    public void onResume() {
      super.onResume();

      new DatabaseTask().execute(activity, this);
    }

    @Override
    public void onDestroy() {
      super.onDestroy();

      if (activity.getActionBar() != null) {
        activity.getActionBar().setDisplayHomeAsUpEnabled(false);
      }
    }

    private static class DatabaseTask extends AsyncTask<Object, Object, Cursor> {
      private Context context;
      private ListFragment listFragment;

      private long totalTimeLength = 0; //in minutes

      @Override
      protected Cursor doInBackground(Object[] params) {
        context = (Context) params[0];
        listFragment = (ListFragment) params[1];
        SQLiteOpenHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
//read logs from db
        String[] columns = {ID, NAME, START_TIME, TIME_LENGTH};
        String selection = START_TIME + ">?";
        String[] selectionArgs = {String.valueOf(calendar.getTimeInMillis())};
        String orderBy = ID + " ASC";
//query database
        Cursor cursor = db.query(TABLE_NAME, columns, selection, selectionArgs,
            null, null, orderBy);
//debug
        if (DEBUG) {
          Log.d("Cursor is", String.valueOf(cursor.getCount()));
        }
//if no data
        if (cursor.getCount() == 0)
          return null;
//dealing with data
        MatrixCursor result = new MatrixCursor(new String[]{"_id", NAME, "log"});
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        Calendar current = Calendar.getInstance();

        cursor.moveToFirst();
        do {
          totalTimeLength += cursor.getInt(3);
          current.setTimeInMillis(cursor.getLong(2));
          result.addRow(new String[]{
              cursor.getString(0),
              cursor.getString(1),
              dateFormat.format(current.getTime())
                  + " -> "
                  + String.format(context.getString(R.string.minutes), cursor.getInt(3))
          });
        } while (cursor.moveToNext());
//close cursor, db and dbHelper
        cursor.close();
        db.close();
        dbHelper.close();

        return result;
      }

      @SuppressWarnings("deprecation")
      @Override
      protected void onPostExecute(Cursor result) {
        TextView header = new TextView(context);
        header.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        header.setTextAppearance(context, android.R.style.TextAppearance_Holo_Large);
        header.setGravity(Gravity.CENTER_HORIZONTAL);

        if (result == null) { //no data
          header.setText(context.getResources().getQuantityText(R.plurals.job, 0));
        }
        else {
          header.setText(
              context.getResources().getQuantityString(
                  R.plurals.job,
                  result.getCount(),
                  result.getCount()
              )
                  + " -> "
                  + String.format(
                  context.getString(R.string.minutes),
                  totalTimeLength
              )
          );
//add header
          if (listFragment.getListView().getHeaderViewsCount() == 0) {
            listFragment.getListView().addHeaderView(header);
          }
//add data
          listFragment.setListAdapter(new SimpleCursorAdapter(
              context,
              android.R.layout.two_line_list_item,
              result,
              new String[]{NAME, "log"},
              new int[]{android.R.id.text1, android.R.id.text2}
          ));
        }
      }
    }
  }
}
