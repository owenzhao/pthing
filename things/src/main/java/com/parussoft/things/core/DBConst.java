package com.parussoft.things.core;

/**
 * Created by 肇鑫 (Owen Zhao) on 14-1-6.
 * database constants
 */
public final class DBConst {

  private DBConst() {}

  public static final String TABLE_NAME = "history";
  public static final String ID = "id";
  public static final String NAME = "name";
  public static final String START_TIME = "start_time";
  public static final String TIME_LENGTH = "time_length";
}
