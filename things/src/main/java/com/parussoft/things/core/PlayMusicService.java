package com.parussoft.things.core;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;

import com.parussoft.things.R;

/**
 * Created by 肇鑫 (Owen Zhao) on 13-12-31.
 * Play a notification when time is up
 */
public class PlayMusicService extends Service {
  //private Intent intent = null;

  @Override
  public void onCreate() {
    //Do some preparation here. It this case, we need to nothing here.
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    //get intent for release wake lock
    //this.intent = intent;

    // For each start request, send a message to start a job and deliver the
    // start ID so we know which request we're stopping when we finish the job
    new PlayMusicTask().execute(startId);

    // If we get killed, after returning from here, restart
    return START_STICKY;
  }

  /**
   * Return the communication channel to the service.  May return null if
   * clients can not bind to the service.  The returned
   * {@link android.os.IBinder} is usually for a complex interface
   * that has been <a href="{@docRoot}guide/components/aidl.html">described using
   * aidl</a>.
   * <p/>
   * <p><em>Note that unlike other application components, calls on to the
   * IBinder interface returned here may not happen on the main thread
   * of the process</em>.  More information about the main thread can be found in
   * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
   * Threads</a>.</p>
   *
   * @param intent The Intent that was used to bind to this service,
   *               as given to {@link android.content.Context#bindService
   *               Context.bindService}.  Note that any extras that were included with
   *               the Intent at that point will <em>not</em> be seen here.
   * @return Return an IBinder through which clients can call on to the
   * service.
   */
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  private class PlayMusicTask extends AsyncTask<Integer, Object, Object>
      implements AudioManager.OnAudioFocusChangeListener,
      MediaPlayer.OnCompletionListener {
    private MediaPlayer mediaPlayer = null;
    private AudioManager audioManager = null;
    private int serviceId;

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Object doInBackground(Integer... params) {
      //get service id, so you can stop it correctly
      serviceId = params[0];
      //prepare MediaPlayer
      mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.rest_alarm);

      //get audio focus
      audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      int result = audioManager.requestAudioFocus(this,
          AudioManager.STREAM_ALARM, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
      if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
        playMusic();
      }

      return null;
    }

    private void playMusic() {
      mediaPlayer.start(); //play a song
      mediaPlayer.setOnCompletionListener(this);
    }

    private void releaseResource() {
      if (mediaPlayer != null) {
        //release MediaPlayer resource
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;

        //release audio focus
        audioManager.abandonAudioFocus(this);

        // Stop the service using the startId, so that we don't stop
        // the service in the middle of handling another job
        stopSelf(serviceId);
      }
    }

    /**
     * Called on the listener to notify it the audio focus for this listener has been changed.
     * The focusChange value indicates whether the focus was gained,
     * whether the focus was lost, and whether that loss is transient, or whether the new focus
     * holder will hold it for an unknown amount of time.
     * When losing focus, listeners can use the focus change information to decide what
     * behavior to adopt when losing focus. A music player could for instance elect to lower
     * the volume of its music stream (duck) for transient focus losses, and pause otherwise.
     *
     * @param focusChange the type of focus change, one of {@link android.media.AudioManager#AUDIOFOCUS_GAIN},
     *                    {@link android.media.AudioManager#AUDIOFOCUS_LOSS}, {@link android.media.AudioManager#AUDIOFOCUS_LOSS_TRANSIENT}
     *                    and {@link android.media.AudioManager#AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK}.
     */
    @Override
    //this will never run as IntentService kill
    public void onAudioFocusChange(int focusChange) {
      switch (focusChange) {
        //get audio focus, you can resume your song here
        case AudioManager.AUDIOFOCUS_GAIN:
          if (!mediaPlayer.isPlaying()) playMusic();
          break;
        //lost audio focus
        case AudioManager.AUDIOFOCUS_LOSS:
          releaseResource();
          break;
        //lost audio focus but will be back soon;
        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
          if (mediaPlayer.isPlaying()) mediaPlayer.pause();
          break;
        //lower music volume;
        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
          if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.2f, 0.2f);
          break;
      }
    }

    /**
     * Called when the end of a media source is reached during playback.
     *
     * @param mp the MediaPlayer that reached the end of the file
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
      releaseResource();
    }
  }
}
