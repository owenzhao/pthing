package com.parussoft.things.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.parussoft.things.core.DBConst.*;

/**
 * Created by 肇鑫 (Owen Zhao) on 14-1-6.
 */
public class DBHelper extends SQLiteOpenHelper {
  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_FILE_NAME = "history.db";

  private static final String COMMA = ", ";
  private static final String CREATE_TABLE_IF_NOT_EXISTS =
      "CREATE TABLE IF NOT EXISTS ";
  private static final String PRIMARY_KEY = " INTEGER PRIMARY KEY AUTOINCREMENT";
  private static final String TEXT_NOT_NULL = " TEXT NOT NULL";
  private static final String INTEGER_NOT_NULL = " INTEGER NOT NULL";


  public DBHelper(Context context) {
    this(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
  }

  /**
   * Create a helper object to create, open, and/or manage a database.
   * This method always returns very quickly.  The database is not actually
   * created or opened until one of {@link #getWritableDatabase} or
   * {@link #getReadableDatabase} is called.
   *
   * @param context to use to open or create the database
   * @param name    of the database file, or null for an in-memory database
   * @param factory to use for creating cursor objects, or null for the default
   * @param version number of the database (starting at 1); if the database is older,
   *                {@link #onUpgrade} will be used to upgrade the database; if the database is
   *                newer, {@link #onDowngrade} will be used to downgrade the database
   */
  public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
    super(context, name, factory, version);
  }

  /**
   * Called when the database is created for the first time. This is where the
   * creation of tables and the initial population of the tables should happen.
   *
   * @param db The database.
   */
  @Override
  public void onCreate(SQLiteDatabase db) {
    final String sql = CREATE_TABLE_IF_NOT_EXISTS + TABLE_NAME +
        " (" +
        ID + PRIMARY_KEY + COMMA +
        NAME + TEXT_NOT_NULL + COMMA +
        START_TIME + INTEGER_NOT_NULL + COMMA +
        TIME_LENGTH + INTEGER_NOT_NULL +
        ")";

    db.execSQL(sql);
  }

  /**
   * Called when the database needs to be upgraded. The implementation
   * should use this method to drop tables, add tables, or do anything else it
   * needs to upgrade to the new schema version.
   * <p/>
   * <p>
   * The SQLite ALTER TABLE documentation can be found
   * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
   * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
   * you can use ALTER TABLE to rename the old table, then create the new table and then
   * populate the new table with the contents of the old table.
   * </p><p>
   * This method executes within a transaction.  If an exception is thrown, all changes
   * will automatically be rolled back.
   * </p>
   *
   * @param db         The database.
   * @param oldVersion The old database version.
   * @param newVersion The new database version.
   */
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

  }
}
