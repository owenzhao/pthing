package com.parussoft.things.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parussoft.things.BundleConst;
import com.parussoft.things.DialogActivity;
import com.parussoft.things.MainActivity;

/**
 * Created by 肇鑫 (Owen Zhao) on 14-1-6.
 */
public class TimeUpReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
    Intent activityIntent = new Intent(context, DialogActivity.class);
    activityIntent.putExtras(intent);
    activityIntent.setFlags(
        Intent.FLAG_ACTIVITY_NEW_TASK
            | Intent.FLAG_ACTIVITY_CLEAR_TASK
    );
    context.startActivity(activityIntent);

    Intent serviceIntent = new Intent(context, PlayMusicService.class);
    context.startService(serviceIntent);
//debug
    if (MainActivity.DEBUG) {
      Bundle bundle = intent.getExtras();
      long endTime = bundle.getLong(BundleConst.END_TIME);
      int timeLength = bundle.getInt(BundleConst.TIME_LENGTH);
      Log.d("endTime in receiver is", String.valueOf(endTime));
      Log.d("timeLength in receiver is ", String .valueOf(timeLength));
    }
  }
}
