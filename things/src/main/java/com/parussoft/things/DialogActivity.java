package com.parussoft.things;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.parussoft.things.core.DBConst;
import com.parussoft.things.core.DBHelper;

import static com.parussoft.things.BundleConst.END_TIME;
import static com.parussoft.things.core.DBConst.ID;
import static com.parussoft.things.core.DBConst.NAME;
import static com.parussoft.things.core.DBConst.START_TIME;
import static com.parussoft.things.core.DBConst.TABLE_NAME;
import static com.parussoft.things.core.DBConst.TIME_LENGTH;

public class DialogActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dialog);
//add fragment to activity
    if (savedInstanceState == null) {
      PlaceholderFragment dialog = new PlaceholderFragment();
      dialog.setCancelable(false);
      dialog.setStyle(DialogFragment.STYLE_NO_TITLE,
          android.R.style.Theme_Holo_Light_Dialog_MinWidth);
      dialog.setArguments(getIntent().getExtras());
      dialog.show(getFragmentManager(), "dialog");
    }
//wake up the phone and show dialog on top of all windows
    unlockScreen();
  }

  private void unlockScreen() {
    Window window = getWindow();
    window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
  }

  /**
   * A placeholder fragment containing a simple view.
   */
  public static class PlaceholderFragment extends DialogFragment {
    private static CharSequence lastInput = null;

    private Activity activity = null;
    private DBHelper dbHelper = null;

    public PlaceholderFragment() {}

    @Override
    public void onAttach(Activity activity) {
      super.onAttach(activity);
      this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//initialize dbHelper
      dbHelper = new DBHelper(activity);
//get views
      View view = inflater.inflate(R.layout.dialog_view, container, false);
      final AutoCompleteTextView jobNameAutoCompleteTextView =
          (AutoCompleteTextView) view.findViewById(R.id.job_name_autoCompleteTextView);
//query names for auto-completion
      jobNameAutoCompleteTextView.setAdapter(getJobNamesAdapter());
//fill jobNameAutoCompleteTextView with last input
      if (!TextUtils.isEmpty(lastInput)) {
        jobNameAutoCompleteTextView.setText(lastInput);
      }
//set buttons
      final Button okButton = (Button) view.findViewById(R.id.ok_button);
      final Button cancelButton = (Button) view.findViewById(R.id.cancel_button);

      okButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (checkJobName(jobNameAutoCompleteTextView.getText())) {
            lastInput = jobNameAutoCompleteTextView.getText();
//save job
            saveToDatabase(jobNameAutoCompleteTextView.getText().toString());
            buttonClick();
          }
          else {
//no input
            Toast toast = Toast.makeText(
                activity,
                R.string.no_input_error,
                Toast.LENGTH_LONG);
            toast.show();
          }
        }
      });

      cancelButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          buttonClick();
        }
      });

      return view;
    }

    @Override
    public void onDestroy() {
      super.onDestroy();
//close dbHelper if needed (user presses cancel button instead of ok button)
      if (dbHelper != null) {
        dbHelper.close();
        dbHelper = null;
      }
    }

    private ArrayAdapter<String> getJobNamesAdapter() {
//query database for names
      SQLiteDatabase db = dbHelper.getReadableDatabase();
      boolean distinct = true;
      String[] columns = {DBConst.NAME};
      String orderBy = DBConst.START_TIME + " DESC";
      Cursor cursor = db.query(distinct, DBConst.TABLE_NAME, columns, null, null,
          null, null, orderBy, null);
      String[] name = new String[cursor.getCount()];
      if (cursor.getCount() != 0) {
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
          name[i] = cursor.getString(0);
          if (cursor.getPosition() < cursor.getCount() - 1) {
            cursor.moveToNext();
          }
        }
      }
//close cursor, db but not dbHelper
      cursor.close();
      db.close();

      return new ArrayAdapter<>(activity,
          android.R.layout.simple_dropdown_item_1line, name);
    }

    private boolean checkJobName(CharSequence charSequence) {
      return !TextUtils.isEmpty(charSequence);
    }

    private long saveToDatabase(String name) {
      long result = -1;
//get timeLength in minutes and endTime in UTC milliseconds
      Bundle args = getArguments();
      if (args != null) {
        int timeLength = args.getInt(TIME_LENGTH, 0);
        long endTime = args.getLong(END_TIME, 0);
//save to database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, name);
        contentValues.put(START_TIME, endTime - timeLength * 60 * 1000);
        contentValues.put(TIME_LENGTH, timeLength);
        result = db.insert(TABLE_NAME, ID, contentValues);
        db.close();
      }
//DEBUG
      if (MainActivity.DEBUG) {
        Log.d("save to db is ", (result == -1) ? "failed." : "OK.");
      }
      return result; //-1 means error
    }

    private void buttonClick() {
//dismiss(hide) dialog
      this.dismiss();
//reset window flags, exit activity and start main activity
      lockScreen();
      activity.finish();
      Intent activityIntent = new Intent(activity, MainActivity.class);
      startActivity(activityIntent);
    }

    private void lockScreen() {
      Window window = activity.getWindow();
      window.clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
      window.clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
      window.clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }
  }
}
